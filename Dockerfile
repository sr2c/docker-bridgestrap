FROM golang:1.17-bullseye

COPY tor.list /etc/apt/sources.list.d/
RUN wget -qO- https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --dearmor | tee /usr/share/keyrings/tor-archive-keyring.gpg >/dev/null
RUN apt update
RUN apt install -y tor obfs4proxy


COPY bridgestrap/* /usr/src/bridgestrap/
WORKDIR /usr/src/bridgestrap
# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY bridgestrap/go.mod bridgestrap/go.sum ./
RUN go mod download && go mod verify

RUN go build -v -o /usr/local/bin/bridgestrap ./...
EXPOSE 5000
CMD ["bridgestrap"]
